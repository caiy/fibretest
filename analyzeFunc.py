import matplotlib.pyplot as plt
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn import metrics
from scipy.optimize import curve_fit
import ROOT

def plotDistribution(x, y, outname):
    plt.plot(x, y)
    if outname != '':
        plt.savefig(outname+'.png')
    plt.clf()

def autoRange(x, y):
    x_new = []
    y_new = []
    xlow = 0
    xhigh = 0
    for i in range(len(x)) :
        #print(y[i])
        if y[i] != 0 :
            xlow = i
            break
    for i in reversed(range(len(x))) :
        #print(y[i])
        if y[i] != 0 :
            xhigh = i
            break
    for i in range(xlow-1,xhigh+1):
        x_new.append(x[i])
        y_new.append(y[i])
    return x_new,y_new

def filterYRange(y, y_min=-10, y_max=10):
    y_new = []
    for i in range(len(y)) :
        #print(y[i])
        if y[i] < y_min or y[i] > y_max :
            y_new.append(y[i])
        else:
            y_new.append(0)
    return y_new

def averageBinWidth(x):
    x_new = []
    for i in range(1,len(x)) :
        x_new.append( x[i] - x[i-1] )
    return np.mean(x_new)

def rearrangeXPosition(x):
    x_new = [ x[0] ]
    for i in range(1,len(x)) :
        x_new.append( x[i] - x[0] )
    return x_new

def DBSCANonXY(df,eps=5, min_samples=2):
    db = DBSCAN(eps=eps, min_samples=min_samples).fit(df)
    labels = db.labels_
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)
    print("Estimated number of clusters: %d" % n_clusters_)
    print("Estimated number of noise points: %d" % n_noise_)
    return labels


class fitToGaussian:
    def __init__(self):
        self.coeff=[]
        self.var_matrix=[]
        self.min_x=0
        self.max_x=0
        self.df_x=[]
        self.df_y=[]
        self.x=[]
        self.y=[]

    def gauss_function(self,x, a, x0, sigma):
        return a*np.exp(-(x-x0)**2/(2*sigma**2))
    
    def fit(self, df ):
        df_T = np.array(df).T.tolist()
        y = np.array(df_T[1])
        z = np.array(df_T[2])
        self.df_x = y
        self.df_y = z
        self.min_x = min(y)
        self.max_x = max(y)
        mean = sum(y*z)/sum(z)
        sigma = np.sqrt(sum(z * (y - mean)**2)/sum(z))
        self.coeff, self.var_matrix = curve_fit(self.gauss_function, xdata = y, ydata = z, p0 = [1, mean, sigma] )
        print(self.coeff)
        return self.coeff, self.var_matrix

    def predict(self, gaussian_x = []):
        if len(gaussian_x) > 0 :
            y_ = self.gauss_function(gaussian_x,self.coeff[0],self.coeff[1],self.coeff[2])
        else:
            gaussian_x = np.linspace(self.min_x-1,self.max_x+1,50)
            y_ = self.gauss_function(gaussian_x,self.coeff[0],self.coeff[1],self.coeff[2])
        self.y = y_
        self.x = gaussian_x
        return gaussian_x, y_

    def draw(self, outputname):
        ax = plt.figure().add_subplot()
        ax.scatter(self.df_x, self.df_y )
        ax.plot(self.x, self.y, color='g' )
        plt.savefig(outputname+'.png')
        plt.clf()
        pass


class toTH1:
    def __init__(self, x,y,binwidth=1):
        self._x = x
        self._y = y
        self._binwidth = binwidth
        x_max = max(x)
        self.n_bins = x_max//binwidth + 1
        print(int(self.n_bins),float(self.n_bins * binwidth))
        self._f = ROOT.TH1F('h1', 'h1', int(self.n_bins), 0, float(self.n_bins * binwidth) )
        for i in range(len(x)):
            self._f.Fill(x[i],y[i])
    
    def toTH1(self):
        return self._f
