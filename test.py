from scipy.fft import fft, ifft, fftfreq
import numpy as np
import sys
from array import array
import ROOT
import FindPeak.EstimateNFiredCells as EstimateNFiredCells

import readData
import analyzeFunc

ROOT.gROOT.SetBatch(True)


def usage():
    print("test.py <path_to_input_txt> ")
    sys.exit(2)

def main(args):
    if len(args)<1: usage()

    inputs = readData.readData(sys.argv[1])#"../Data_14_03_2023/1700_X10_Y-100.04_histo.txt")
    datas = inputs.getDataFromTxt(delimiter='\t')
    print(inputs.getDataFromCsvS(inputfiles_list = inputs.searchDataFile("/its/home/yc560/public/FCC/Phrase0/Analysis/output/",".*.csv")))
    
    names = str(inputs).split("_") #1700 X10 Y-100.04 histo.txt"
    outname = str(inputs).split("/")[-1] #1700_X10_Y-100.04_histo.txt
    outname = outname[:-4] #1700_X10_Y-100.04_histo
    x_position = names[1][1:] #10
    y_position = names[2][1:] #-100.4
    
    x_new,y_new = readData.autoRange(datas[0], datas[1])
    
    #analyzeFunc.plotDistribution(datas[0], datas[1], 'test')
    #analyzeFunc.plotDistribution(x_new, y_new, 'test_shorten')
    
    f = EstimateNFiredCells.EstimateNFiredCells(x_new, y_new, float(x_position), float(y_position), 50)
    f.process()
    f.dumpOutput(outname+'.csv')
    f.plot(outname)


if __name__ == "__main__":
    main(sys.argv[1:])


#z_new = fft(y_new)
#z_abs = np.abs(z_new)
#z_angle = np.angle(z_new)
#z_real = z_new.real
#z_imag = z_new.imag
#z_real_iff = ifft(z_real)
#z_abs_iff = ifft(z_abs)
#z_imag_iff = ifft(z_imag)
#analyzeFunc.plotDistribution(x_new, z_new  , 'test_fft')
#analyzeFunc.plotDistribution(x_new, z_abs  , 'test_fft_abs'  )
#analyzeFunc.plotDistribution(x_new, z_angle, 'test_fft_angle')
#analyzeFunc.plotDistribution(x_new, z_real , 'test_fft_real' )
#analyzeFunc.plotDistribution(x_new, z_imag , 'test_fft_imag' )
#analyzeFunc.plotDistribution(x_new, z_real_iff , 'test_fft_real_iff' )
#analyzeFunc.plotDistribution(x_new, z_abs_iff , 'test_fft_abs_iff' )
#analyzeFunc.plotDistribution(x_new, z_imag_iff , 'test_fft_imag_iff' )

#z_real_filtered = analyzeFunc.filterYRange(z_new, -200, 300 )
#analyzeFunc.plotDistribution(x_new, z_real_filtered , 'test_fft_real_filtered' )
#z_real_iff_filtered = ifft(z_real_filtered)
#analyzeFunc.plotDistribution(x_new, z_real_iff_filtered , 'test_fft_real_filtered_iff' )
