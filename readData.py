import numpy as np
import os
import re


class readData:
    def __init__(self, inPath):
        self.inPath = inPath

    def __str__(self):
        return self.inPath.split("/")[-1]

    def searchDataFile(self, inPath, file_pattern):
        output_list = []
        for each_file in os.listdir(inPath):
            x = re.search(file_pattern, each_file)
            if x:
                output_list.append(inPath + each_file)
        return output_list

    def getDataFromTxt(self, delimiter='\t',dtype='float', alternativepath=''):
        if alternativepath == '' :
            return np.loadtxt(self.inPath, dtype=dtype, delimiter=delimiter ).T
        else :
            return np.loadtxt(alternativepath, dtype=dtype, delimiter=delimiter ).T

    def getDataFromCsv(self, delimiter=',',dtype='object', alternativepath=''):
        if alternativepath == '' :
            return np.loadtxt(self.inPath, delimiter=delimiter, dtype=dtype)
        else :
            return np.loadtxt(alternativepath, delimiter=delimiter, dtype=dtype)

    def getDataFromCsvS(self, inputfiles_list, delimiter=',',dtype='object'):
        Csv_list = []
        for i in inputfiles_list:
            tempCsv = self.getDataFromCsv( alternativepath = i, delimiter = delimiter, dtype = dtype)
            for j in tempCsv:
                Csv_list.append(j)
        return Csv_list


def autoRange(x, y):
    """
    Remove lower and higher points where y=0.
    """
    x_new = []
    y_new = []
    xlow = 0
    xhigh = 0
    for i in range(len(x)) :
        #print(y[i])
        if y[i] != 0 :
            xlow = i
            break
    for i in reversed(range(len(x))) :
        #print(y[i])
        if y[i] != 0 :
            xhigh = i
            break
    for i in range(xlow-1,xhigh+1):
        x_new.append(x[i])
        y_new.append(y[i])
    return x_new,y_new


def rearrangeXPosition(x):
    """
    Reset x value to relative value.
    """
    x_new = [ x[0] ]
    for i in range(1,len(x)) :
        x_new.append( x[i] - x[0] )
    return x_new
