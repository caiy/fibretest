from abc import ABCMeta, abstractmethod

class FindPeak(metaclass=ABCMeta):
    def __init__(self):
        pass
    
    @abstractmethod
    def read(self, **kwargs):
        pass

    @abstractmethod
    def process(self, **kwargs):
        pass

    @abstractmethod
    def dumpOutput(self, **kwargs):
        pass

    @abstractmethod
    def plot(self, **kwargs):
        pass

