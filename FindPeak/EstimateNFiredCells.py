from array import array
import ROOT
import csv

from FindPeak import FindPeak
import readData
import analyzeFunc

ROOT.gROOT.SetBatch(True)

class EstimateNFiredCells(FindPeak.FindPeak):
    def __init__(self,x_new, y_new, x_posi, y_posi, rebinNumber = 50):
        """
        Receive data inputs, rebin it, and convert it to ROOT.TH1F
        """
        super().__init__()
        self.histogram = self.read(x_new, y_new, rebinNumber)
        self.x_points, self.y_points = array( 'd' ), array( 'd' )
        self.x_posi = x_posi
        self.y_posi = y_posi
        self.results = {}
    
    def read(self, x_new, y_new, rebinNumber):
        f = analyzeFunc.toTH1(x_new, y_new, rebinNumber).toTH1()
        return f

    def process(self):
        """
        Loop through Bins
        """
        for i in range(3,self.histogram.GetNbinsX()-3):
            if ( self.histogram.GetBinContent(i) > self.histogram.GetBinContent(i-1) 
                and self.histogram.GetBinContent(i) > self.histogram.GetBinContent(i-2) 
                and self.histogram.GetBinContent(i) > self.histogram.GetBinContent(i+1) 
                and self.histogram.GetBinContent(i) > self.histogram.GetBinContent(i+2)
                and (self.histogram.GetBinContent(i+1)+self.histogram.GetBinContent(i+2)) * (self.histogram.GetBinContent(i-1)+self.histogram.GetBinContent(i-2)) > 0
                ):
                """
                peak: f(i) > f(i-2) to f(i+2) 
                    and f(i+1) + f(i+2) > 0
                    and f(i-1) + f(i-2) > 0
                """
                print(self.histogram.GetBinCenter(i))
                self.x_points.append(self.histogram.GetBinCenter(i))
                self.y_points.append(self.histogram.GetBinContent(i))
        yiADCi=0
        yi=0
        x_points_rearrange=readData.rearrangeXPosition(self.x_points) # get x relative distance
        for i in range(len(self.x_points)):
            yiADCi += x_points_rearrange[i]*self.y_points[i]
            yi += self.y_points[i]
        averADC = yiADCi/yi # total ACD channels of all Photon received
        averPP = analyzeFunc.averageBinWidth(self.x_points) # average PP distance in ADC channels
        print(">>>>>>",averADC/averPP,averADC,averPP)
        self.results["NFCells"] = averADC/averPP
        self.results["totalADC"] = averADC
        self.results["ppADC"] = averPP
    
    def dumpOutput(self, outputname ):
        labels = ['X', 'Y']
        values = [self.x_posi, self.y_posi]
        for key,value in self.results.items():
            labels.append(key)
            values.append(value)
        with open(outputname, mode='w') as employee_file:
            employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            employee_writer.writerow(labels)
            employee_writer.writerow(values)
        return values

    def plot(self,outputname):
        g = ROOT.TGraph(int(len(self.x_points)), self.x_points, self.y_points)
        g.SetMarkerColor(2);
        #f=f.Rebin(10)
        c = ROOT.TCanvas()
        self.histogram.Draw();
        g.Draw("P* same")
        c.SaveAs(outputname+".png");    
